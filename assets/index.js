console.log('entrée js');
const element = document.querySelector('.titre');
const borobudur = document.querySelector('.borobudur');
const bromo = document.querySelector('.bromo');

element.classList.add('animate__animated', 'animate__zoomin');
element.style.setProperty('--animate-duration', '2s');
let btninfo = document.getElementById('affinfo');
// initialisation de la carte à l'affichage d'une ligne centrée sur borobudur
const mymapboro = L.map('mapboro').setView([-7.6086, 110.2044], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibXlyaWFtc2ltcGxvbiIsImEiOiJja2Vqdmh1YmgxYnQ3MzFxYmFnc3UyMnd5In0.zhrTSulbycCCVOo0DctSww', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoibXlyaWFtc2ltcGxvbiIsImEiOiJja2Vqdmh1YmgxYnQ3MzFxYmFnc3UyMnd5In0.zhrTSulbycCCVOo0DctSww'
})
    .addTo(mymapboro);
let marker = L.marker([-7.6086, 110.2044]).addTo(mymapboro);
// // initialisation de la carte à l'affichage d'une ligne centrée sur volcan bromo
// const mymapbromo = L.map('mapbromo').setView([-7.9617, 112.9489], 13);

// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibXlyaWFtc2ltcGxvbiIsImEiOiJja2Vqdmh1YmgxYnQ3MzFxYmFnc3UyMnd5In0.zhrTSulbycCCVOo0DctSww', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     id: 'mapbox/streets-v11',
//     accessToken: 'pk.eyJ1IjoibXlyaWFtc2ltcGxvbiIsImEiOiJja2Vqdmh1YmgxYnQ3MzFxYmFnc3UyMnd5In0.zhrTSulbycCCVOo0DctSww'
// })
//     .addTo(mymapbromo);
// let markermymapbromo = L.marker([-7.9617, 112.9489]).addTo(mymapbromo);

$(document).ready(function () {
    btninfo.addEventListener('click', function () {
        $('#infos').modal('show');

    })
    $('#borobudur').hide();
    borobudur.addEventListener('click', function () {
        $('#bromo').hide();
        $('#borobudur').show();
	
    }) 
    $('#bromo').hide();
    bromo.addEventListener('click', function () {
        $('#borobudur').hide();
        $('#bromo').show();
	
	}) 
    $('.carousel').slick({
        dots: true,
        infinite: false,
        centerMode: true,
        autoplay: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});
